#define _OPEN_SYS_ITOA_EXT
#include <stdio.h>
#include <stdlib.h>
#include "BigNumber.h"
#include <iostream>


using namespace std;

bool test(uint64_t expected, string actual, string testName)
{
	if (to_string(expected) == actual)
	{
		cout << testName <<  " PASSED!\n";
		return true;
	}
	else
	{
		cout << testName << " FAILED!!!" << endl;
		cout << "Expected " << expected << " but was " << actual << endl;
		return false;
	}
}

void testSum()
{
	uint64_t random1,
		random2;
	srand(time(0));
	MyBigNumber bignum;
	string temp = "";

	test(0, bignum.sum("0", "0"), "special test");


	for (int i = 1; i <= 100; i++)
	{
		random1 = rand();
		random2 = rand();

		if (random1 > random2)
		{
			swap(random1, random2);
		}

		temp = "Test" + to_string(i);
		
		cout << endl;
		if (!test(random2, bignum.sum(to_string(random1), to_string(random2 - random1)), temp))
		{
			cout << random1 << " + " << random2 - random1 << endl;
			break;
		}


	}
	
}

int main()
{
	testSum();
	return 0;
}