﻿#include "BigNumber.h"

using namespace std;

int cong(char a, char b) 
{
	int result = 0;

	result = (a - '0') + (b - '0');

	return result;
}

string MyBigNumber::sum(string stn1, string stn2)
{
	int i = stn1.length() - 1,
		j = stn2.length() - 1,
		nho = 0,
		temp;

	int t = 1;
	string result = "";

	while ((i >= 0 && j >= 0) || nho != 0) 
	{

		cout << "Buoc: " << t << endl;
		if (i < 0 && j < 0)
		{
			cout << "Cong nho " << nho << " vao ket qua duoc ";
			result = (char)(nho + '0') + result;
			nho = 0;
			cout << result << endl;
		}
		else
		{
			if (i < 0)
			{
				cout << "Lay " << stn2[j] << " cong voi nho " << nho << " duoc ";
				temp = (stn2[j] - '0') + nho;
				cout << temp << endl;

			}
			else if (j < 0)
			{
				cout << "Lay " << stn1[i] << " cong voi nho " << nho << " duoc ";
				temp = (stn1[i] - '0') + nho;
				cout << temp << endl;
			}
			else
			{
				cout << "Lay " << stn1[i] << " cong voi " << stn2[j] << " duoc ";
				temp = (stn1[i] - '0') + (stn2[j] - '0');
				cout << temp << ".";
				if (nho != 0)
				{
					cout << " Cong tiep voi nho " << nho << " duoc ";
					temp += nho;
					cout << temp;
				}
				cout << endl;
			}

			cout << "Luu " << temp % 10 << " vao ket qua ";
			result = (char)(temp % 10 + '0') + result;
			nho = temp / 10;
			cout << "va nho " << nho << endl;

			i--;
			j--;
			t++;
		}
		
	}

	if (i >= 0)
	{
		cout << "Buoc " << t << endl;
		result = stn1.substr(0, i + 1) + result;
		cout << "Lay " << stn1.substr(0, i + 1) << " xuong them vao ket qua duoc " << result << endl;

	}
	else if (j >= 0)
	{
		cout << "Buoc " << t << endl;
		result = stn2.substr(0, j + 1) + result;
		cout << "Lay " << stn2.substr(0, j + 1) << " xuong them vao ket qua duoc " << result << endl;
	}

	cout << "Ket qua: " << result << endl;

	return string(result);
}
