## Tổng 2 số lớn
#### Chương trình gồm:
- File BigNumber.h, BigNumber.cpp: chứa đoạn mã thực hiện tính tổng 2 số.
- File Main.cpp chứa đoạn mã test hàm tính tổng 2 số lớn.
#### Cách chạy chương trình:
- Clone repository https://gitlab.com/ngoxuanchien/tong2solon.git thông qua Visual Studio 2022
- Run file main
